load 'config.rb'
require 'trello'
require_relative 'mailer'

TRELLO_DEVELOPER_PUBLIC_KEY = ''
TRELLO_MEMBER_TOKEN = ''

BOARD_NAME = 'https://trello.com/b/iPcddlnU/casetracking'

LIST_NEXT_WEEK = 4
LIST_CURRENT_WEEK = 5
LIST_DONE = 6
LIST_STAGING = 7

Trello.configure do |trello|
  trello.developer_public_key = TRELLO_DEVELOPER_PUBLIC_KEY
  trello.member_token = TRELLO_MEMBER_TOKEN
end

board = Trello::Board.all.detect do |b|
  b.url == BOARD_NAME
end

commits = []

board.lists[LIST_CURRENT_WEEK].cards.each do |c|
  commits.push name: c.name, url: c.short_url
end

board.lists[LIST_DONE].cards.each do |c|
  commits.push name: c.name, url: c.short_url
end

email = Mailer.commit_email(commits)

email.deliver_now
